package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Response struct {
	Message string `json:"message"`
	Data    string `json:"data"`
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	response := Response{
		Message: "Hello",
		Data:    "Go Experiments!!",
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(response); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func callNodeApiHandler(w http.ResponseWriter, r *http.Request) {
	// Make a GET request to the Node.js server
	response, err := http.Get("http://localhost:8091/view")
	if err != nil {
		http.Error(w, fmt.Sprintf("Error calling Node.js server: %s", err), http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	// Read the response body
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("Error reading response body: %s", err), http.StatusInternalServerError)
		return
	}

	// Set the response content type
	w.Header().Set("Content-Type", "application/json")

	// Write the response from the Node.js server to the client
	w.WriteHeader(response.StatusCode)
	w.Write(body)
}

func main() {
	http.HandleFunc("/view/", viewHandler)
	http.HandleFunc("/call-node", callNodeApiHandler)
	http.ListenAndServe(":8090", nil)
}
